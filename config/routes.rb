Rails.application.routes.draw do
  
  
  devise_for :users
  resources :categories
  
  resources :posts do
    resources :comments
  end

  root 'pages#home'
  
  get 'about',to: 'pages#about'
  get 'contact', to: 'pages#contact'
  get 'blog', to: 'pages#blog'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
