class Category < ApplicationRecord
  validates :name, presence: true, uniqueness: {case_sensitive: false}, length: {minimum: 3, maximum: 240}
  validates :content, presence: true, length:{minimum: 7, maximum: 1000}

  has_many :posts
end
