class Post < ApplicationRecord
  validates :title, presence: true, uniqueness: {case_sensitive: false}, length: {minimum: 3, maximum: 240}
  validates :content, presence: true, length: {minimum: 7, maximum: 15000}

  belongs_to :user
  belongs_to :category
  has_many :comments, dependent: :destroy

  # Model for Search result
  def self.search(search)
    if search
      where(["title LIKE ?", "%#{search}%"]).order('created_at DESC')
    else
      all.order('created_at DESC')
    end
  end

  has_one_attached :thumbnail
end
