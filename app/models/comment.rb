class Comment < ApplicationRecord
  validates :name, presence: true, length: {minimum: 3, maximmum: 255}
  validates :message, presence: true, length: {minimum: 7, maximum: 2000}
  belongs_to :post
end
