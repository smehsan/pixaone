// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .
//= require tinymce-jquery


$( document ).on('turbolinks:load', function() {
  tinyMCE.init({
    selector: 'textarea.tinymce',
    forced_root_block : "",
    force_br_newlines : true,
  });

  $(".content").each(function(){
    if ($(this).text().length > 150) {
      $(this).text($(this).text().substr(0, 150));
      $(this).append('...');
    }
  });
});