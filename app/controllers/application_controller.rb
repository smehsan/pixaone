class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    added_attrs = [:username, :email, :password, :password_confirmation, :remember_me] # This line sets a variable added_attrs with the attributes
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs # This line says use the attributes for signup
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs # This line says use the attributes for update account
  end
end

# Lets open up config/initializers/devise.rb line 43