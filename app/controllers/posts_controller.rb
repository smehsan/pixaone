class PostsController < ApplicationController
  before_action :set_postid, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show]

  def index
    @posts = Post.where(:user_id => current_user.id)
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user
    if @post.save
      flash[:success] = "Post created successfully."
      redirect_to post_path(@post)
    else
      render 'new'
    end
  end

  def show
  end

  def edit
    unless current_user.id == @post.user_id
      flash[:danger] = "You're not allowed to edit someone else post ?"
      redirect_to post_path(@post)
    end
  end

  def update
    if @post.update(post_params)
      flash[:success] = "Post updated successfully."
      redirect_to post_path(@post)
    else
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    flash[:danger] = "Post deleted successfully"
    redirect_to posts_path
  end

  private
  def post_params
    params.require(:post).permit(:title, :content, :category_id, :thumbnail)
  end

  def set_postid
    @post = Post.find(params[:id])
  end
end
