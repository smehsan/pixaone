class CategoriesController < ApplicationController
  before_action :cat_find, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @cats = Category.all
  end

  def new
    @cat = Category.new
  end

  def create
    @cat = Category.new(cat_params)
    if @cat.save
      flash[:success] = "Category created successfully."
      redirect_to categories_path
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @cat.update(cat_params)
      flash[:success] = "Category updated successfully."
      redirect_to categories_path
    else
      render 'edit'
    end
  end

  def destroy
    @cat.destroy
    flash[:danger] = "Category deleted successfully"
    redirect_to categories_path
  end


  private
  def cat_params
    params.require(:category).permit(:name, :content)
  end

  def cat_find
    @cat = Category.find(params[:id])
  end
end
