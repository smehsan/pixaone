class PagesController < ApplicationController
  def home
    @posts = Post.all.order('created_at DESC').limit(4)
  end

  def about
  end

  def contact
  end

  def blog
    # @posts = Post.all.paginate(:page => params[:page], :per_page => 3)
    @posts = Post.search(params[:search]).paginate(:page => params[:page], :per_page => 3)
  end
end
